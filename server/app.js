    var express = require('express'); 
    var app = express(); 
    var bodyParser = require('body-parser');
    var multer = require('multer');
    var jsonfile = require('jsonfile');
    var filePath = '../client/uploads/flats.json';
    var fileObj = require(filePath);
    var id = '';
    var nameLoadsFile = '';
    var pathLoadsImages = '../client/uploads/img/';

    app.use(function(req, res, next) { //allow cross origin requests
        res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
        res.header("Access-Control-Allow-Origin", "http://localhost");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    /** Serving from the same express Server
    No cors required */
    app.use(express.static('../client'));
    app.use(bodyParser.json());  

    var storage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
            cb(null, pathLoadsImages);
        },
        filename: function (req, file, cb) {
            id =  Date.now();
            nameLoadsFile = file.fieldname + '-' + id + '.' + file.originalname.split('.')[file.originalname.split('.').length -1];
            cb(null, nameLoadsFile);
        }
    });

    var upload = multer({ //multer settings
                    storage: storage
                }).single('file');

    /** API path that will upload the files */
    app.post('/uploadImg', function(req, res) {
        upload(req,res,function(err){

            if(err){
                 res.json({error_code:1,err_desc:err});
                 return;
            }
             res.json({error_code:0,err_desc:null, idObj: id, nameImg: nameLoadsFile });
        });
    });

    app.post('/insertData', function(req, res) {

        var jsonData = JSON.parse(JSON.stringify(req.body));

        var saveImageUrl = '/uploads/img/'+jsonData['imageUrl'];

        jsonData['imageUrl'] = saveImageUrl;

        fileObj.push(jsonData);
        jsonfile.writeFileSync(filePath, fileObj);

        //save detail

        jsonData['imageUrl'] = [saveImageUrl];
        jsonfile.writeFileSync('../client/uploads/'+jsonData['id']+'.json', jsonData);
        res.end( "Saved");
    });

    //Update .jsonFile (add image url)

    app.post('/updateData', function(req, res) {

        var jsonData = JSON.parse(JSON.stringify(req.body));
        var saveImageUrl = '/uploads/img/'+jsonData['imageUrl'];
        var pathUpdateFile = '../client/uploads/'+jsonData['flatId']+'.json';
        var fileUpdate =  require(pathUpdateFile);
        fileUpdate['imageUrl'].push(saveImageUrl);
        jsonfile.writeFileSync(pathUpdateFile, fileUpdate);
        res.end( "Saved");
    });

    app.listen('3000', function(){
        console.log('running on 3000...');
    });