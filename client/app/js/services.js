'use strict';

/* Services */

var flatServices = angular.module('myApp.flatServices', ['ngResource']);

flatServices.factory('Flat', ['$resource',
  function($resource){
    return $resource('http://localhost:3000/uploads/:flatId.json', {}, {
      query: {method:'GET', params:{flatId:'flats'}, isArray:true}
    });
  }]);
