'use strict';

angular.module('myApp.listCtrl', ['ngRoute'])

.controller('ListCtrl', ['$scope', 'Flat',
  function($scope, Flat) {
    $scope.flats = Flat.query();
    $scope.orderProp = 'datetime';

    $scope.setOrderProp = function(prop){
    	$scope.orderProp = prop;
    };
}]);


