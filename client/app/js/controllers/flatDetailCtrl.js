'use strict';

angular.module('myApp.flatDetailCtrl', ['ngFileUpload','ngRoute','ui.bootstrap'])

.controller('FlatDetailCtrl', ['$scope', '$routeParams','Upload', 'Flat','$http',
  function($scope, $routeParams,Upload, Flat, $http) {

$scope.flat = Flat.get({flatId: $routeParams.flatId}, function(flat) {
 $scope.max = 100;
 $scope.dynamic = 0;

  $scope.isCollapsed = true;

      $scope.mainImageUrl = flat.imageUrl[0];
  });
      $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
  }

	$scope.addImg = function(){ //function to call on form submit
        if ($scope.upload_form.file.$valid && $scope.file) { //check if from is valid
            $scope.uploadImg($scope.file); //call upload function
        }
    };

    $scope.uploadImg = function (file) {

            Upload.upload({
            url: 'http://localhost:3000/uploadImg', 
            data:{file:file} //pass file as data, should be user ng-model
        }).then(function (resp) {
            if(resp.data.error_code === 0){ //validate success
            	var jdata = {
            		flatId : $routeParams.flatId,
            		imageUrl : resp.data.nameImg
            	}
            	var res = $http.post('http://localhost:3000/updateData', JSON.stringify(jdata));
            	res.success(function(data, status, headers, config) {
            		$scope.flat = Flat.get({flatId: $routeParams.flatId});
            	});
            } else {
                console.log('an error occured');
            }
        }, function (resp) { //catch error
            console.log('Error status: ' + resp.status);
        }, function (evt) { 
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.dynamic = progressPercentage;
            $scope.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
            console.log(progressPercentage);
        });
    };
}]);
