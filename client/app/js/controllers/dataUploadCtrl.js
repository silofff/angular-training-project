angular.module('dataUploadCtrl', ['ngFileUpload', 'ngRoute']) 

.controller('DataUploadCtrl',['$scope','Upload','$http','$window',
    function($scope, Upload, $http, $window) {
        
    $scope.save = function(){ //function to call on form submit
          console.log('1111');
        
        if ($scope.upload_form.file.$valid && $scope.file) { //check if from is valid
            $scope.uploadImg($scope.file); //call upload function
             console.log('2222');
        };
}
    
    $scope.uploadImg = function (file) {
            Upload.upload({
            url: 'http://localhost:3000/uploadImg', //webAPI 
            data:{file:file}
        }).then(function (resp) { //upload function returns a promise
            if(resp.data.error_code === 0){ //validate success
                $scope.uploadData(resp.data.idObj, resp.data.nameImg);//save form data
            } else {
                console.log('an error occured');
            }
        }, function (resp) { //catch error
            console.log('Error status: ' + resp.status);
        }, function (evt) { 
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
        });
    };

    $scope.uploadData = function (idObj, nameImg) {

    var formData =
    {
        "title": this.titleOrder,
        "id": idObj, 
        "imageUrl": nameImg, 
        "address": this.address,
        "price": this.price,
        "datetime": Date.now(),
        "roomsNumber": this.roomsNumber,
        "email": this.email
    };
    var jdata = JSON.stringify(formData);
    var res = $http.post('http://localhost:3000/insertData', jdata);
    console.log(jdata);

    this.titleOrder = '';
    this.imageUrl = '';
    this.address = '';
    this.price = '';
    this.datetime = '';
    this.email ='';
    this.roomsNumber = '';
    };
}]);
