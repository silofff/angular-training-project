'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.homeCtrl',
  'myApp.listCtrl',
  'myApp.flatDetailCtrl',
  'myApp.version',
  'myApp.flatServices',
  'dataUploadCtrl'
]).
config(['$routeProvider', function($routeProvider) {


  $routeProvider.
      when('/home', {
        templateUrl: 'partials/home.html',
        controller: 'HomeCtrl'
      }).
      when('/flats', {
        templateUrl: 'partials/flat-list.html',
        controller: 'ListCtrl'
      }).
      when('/flats/:flatId', {
        templateUrl: 'partials/flat-detail.html',
        controller: 'FlatDetailCtrl'
      }).
      when('/upload', {
        templateUrl: 'partials/upload.html',
        controller: 'DataUploadCtrl'
      }).
      otherwise({redirectTo: '/home'});
}]);
